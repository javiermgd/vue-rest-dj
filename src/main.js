// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'

import App from './App'
import store from './store/store.js'

/* eslint-disable no-new */
Vue.config.devtools = true
Vue.config.productionTip = false

/* eslint-disable no-new */
var vm = new Vue({
  el: '#app',
  template: '<App>',
  store: store,
  components: { App }
})

// This should be the only new line ***
vm.$store.dispatch('getTodos')
